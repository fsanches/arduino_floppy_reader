#define STEP 2 //pin# 20
#define DIRECTION 3 //pin# 18
#define TRACK00 4 //pin# 26
#define MOTOR 5 //pin# 16
#define INDEX 6 //pin# 08

#define MAX_STEPS 80

void do_step(){
  digitalWrite(STEP, LOW);
  delay(5);
  digitalWrite(STEP, HIGH);
  delay(5);
}

#define FORA HIGH
#define DENTRO LOW

void fora(){
  digitalWrite(DIRECTION, FORA);
  for (int i=0; i<MAX_STEPS; i++)
    do_step();
}

void dentro(){
  digitalWrite(DIRECTION, DENTRO);
  for (int i=0; i<MAX_STEPS; i++)
    do_step();
}

void init_disk_controller(){
//  digitalWrite(MOTOR, HIGH);

  //dentro();
  //fora();

//  digitalWrite(DIRECTION, DENTRO);
 // while (digitalRead(TRACK00) == LOW){
  //  do_step();
  //}

  //fora();
  //dentro();

//  digitalWrite(MOTOR, HIGH);
}

void setup() {
  pinMode(STEP, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  pinMode(INDEX, INPUT);
  pinMode(TRACK00, INPUT);
  pinMode(MOTOR, OUTPUT);

  init_disk_controller();
}

void loop() {
}

